package com.dzaki.DZAKI_1202164350_SI4007_PAB_Modul5.Database;

public class Artikel {
    private String Id,Title,Author,Description,Created_at;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getAuthor() {
        return Author;
    }

    public void setAuthor(String author) {
        Author = author;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getCreated_at() {
        return Created_at;
    }

    public void setCreated_at(String created_at) {
        Created_at = created_at;
    }

    public Artikel(String id, String title, String author, String description, String created_at) {
        Id = id;
        Title = title;
        Author = author;
        Description = description;
        Created_at = created_at;
    }
}
